## Description

Example of customizing a Fedora 18 riak image to use etcd.

### Updating the image

```
docker run -i -t philips/riak /bin/sh -c "mkdir /usr/local/lib/etcd/; curl https://bitbucket.org/coreos/etcd-riak/get/master.tar.gz | tar -C /usr/local/lib/etcd/ --strip=1 -xzv"
docker commit $(docker ps -a | head -n 2 | tail -n 1 | cut -f 1 -d" ") philips/riak
docker push philips/riak
```
